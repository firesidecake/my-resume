import React from 'react';
import { NextPage } from 'next';
import { Box } from '@material-ui/core';

import { useStyles } from '../styles/index.style';
import { Sidebar } from '../features/Sidebar';
import { Home as HomeComponent } from '../features/Home';

const Home: NextPage<any> = () => {
  const classes = useStyles();

  return (
    <Box className={classes.outerContainer}>
      <Box className={classes.innerContainer}>
        <Sidebar />
        <HomeComponent />
      </Box>
    </Box>
  );
};

export default Home;
