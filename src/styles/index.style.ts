import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  outerContainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    overflow: 'auto',
    flex: 1,
  },
  innerContainer: {
    display: 'flex',
    height: '100%',
    overflow: 'auto',
    flexWrap: 'wrap',
  },
});
