import { ReactElement, ReactNode } from 'react';

type Right = {
  value: string | ((props: any) => ReactNode);
  display?: string;
  type?: 'link' | 'email' | 'phone' | 'ReactNode';
};

export interface SideCardData {
  title: string;
  items: {
    left: string;
    leftIcon?: ReactElement;
    right: Right | Right[];
  }[];
}
