import { SideCardData } from '../models/side-card.model';

export const references: SideCardData = {
  title: 'references',
  items: [
    {
      left: 'David Nguyen',
      right: [
        { value: 'Software Manager at LEAP Dev' },
        { value: 'https://www.linkedin.com/in/giangnn/', type: 'link' },
        { value: 'david.nguyen@leapdev.io', type: 'email' },
        {
          value: 'https://drive.google.com/open?id=1Iso3ksq3kplW7a5pE1kkjW71rLhyNKuT',
          display: "David's Reference letter",
          type: 'link',
        },
      ],
    },
    {
      left: 'Christina Chun',
      right: [
        { value: 'Founder of 1Scope' },
        { value: 'Strategic Partnerships and Engagement of NSW Department of Education' },
        { value: 'https://www.linkedin.com/in/christina-chun-4a251b72', type: 'link' },
        { value: 'christina@walker.org.au', type: 'email' },
        { value: '+61 416 910 988', type: 'phone' },
      ],
    },
  ],
};

const sideBarData: SideCardData[] = [
  {
    title: 'contact',
    items: [
      { left: 'Email', right: { value: 'trungdang.uow@gmail.com', type: 'email' } },
      { left: 'Mobile', right: { value: '+61 414 61 9898', type: 'phone' } },
    ],
  },
  {
    title: 'links',
    items: [
      { left: 'LinkedIn', right: { value: 'https://www.linkedin.com/in/trung-dang-amakawa', type: 'link' } },
      { left: 'Github', right: { value: 'https://github.com/allicanseenow/', type: 'link' } },
    ],
  },
  { ...references },
];

export const getSideBarData = () => sideBarData;
