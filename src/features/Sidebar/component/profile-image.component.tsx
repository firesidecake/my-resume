import React from 'react';
import { Avatar, Box, Typography } from '@material-ui/core';

import { useStyles } from '../styles/profile-image.style';

export const ProfileImage = () => {
  const classes = useStyles();

  return (
    <Box className={classes.container}>
      <Avatar alt="Quang Trung Dang's image" src="profile-image.jpg" className={classes.profileImage} />

      <h2 className={classes.name}>Quang Trung Dang</h2>
      <h4 className={classes.jobTitle}>Software developer</h4>
    </Box>
  );
};
