import React from 'react';
import { Box } from '@material-ui/core';

import { useStyles } from '../styles/sidebar.style';
import { ProfileImage, SideInfo } from '.';

export const Sidebar = () => {
  const classes = useStyles();

  return (
    <Box className={classes.sidebar}>
      <ProfileImage />
      <SideInfo />
    </Box>
  );
};
