import React, { useEffect, useState } from 'react';
import { Box, Grid, GridSpacing, Theme, useMediaQuery } from '@material-ui/core';

import { useSideInfoStyles } from '../styles/side-info.style';
import { SideCard } from './side-card.component';
import { getSideBarData } from '../data/sidebar.data';

export const SideInfo = () => {
  const classes = useSideInfoStyles();
  const matches = useMediaQuery((theme: Theme) => theme.breakpoints.between('sm', 'md'));
  const [spacing, setSpacing] = useState<GridSpacing>(0);

  useEffect(() => {
    setSpacing(matches ? 4 : 0);
  }, [matches]);

  return (
    <Box className={classes.container}>
      <Grid container spacing={spacing}>
        {getSideBarData().map((item, index) => {
          const sm = getSideBarData().length === index + 1 ? true : 6;
          return (
            <Grid item xs={12} sm={sm} md={12} key={item.title}>
              <SideCard title={item.title} items={item.items} />
            </Grid>
          );
        })}
      </Grid>
    </Box>
  );
};
