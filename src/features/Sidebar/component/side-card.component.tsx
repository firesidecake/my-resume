import React, { FC } from 'react';
import { Box, Grid, Typography, Link } from '@material-ui/core';

import { useSideCardStyles } from '../styles/side-info.style';
import { SideCardData } from '../models/side-card.model';

const RightContainer: FC<any> = (props) => {
  const { type, value, display, className } = props;
  const displayedValue = display || value;
  switch (type) {
    case 'email':
      return (
        <Link href={`mailto:${value}`} className={className}>
          <Typography variant="body2">{displayedValue}</Typography>
        </Link>
      );
    case 'phone':
      return (
        <Link target="b" href={`tel:${value}`} className={className}>
          <Typography variant="body2">{displayedValue}</Typography>
        </Link>
      );
    case 'link':
      return (
        <Link target="b" href={value} className={className}>
          <Typography variant="body2">{displayedValue}</Typography>
        </Link>
      );
    case 'ReactNode':
      return <div className={className}>{typeof value === 'function' ? value(props) : value}</div>;
    default:
      return (
        <div className={className}>
          <Typography variant="body2">{displayedValue}</Typography>
        </div>
      );
  }
};

export const SideCard: FC<SideCardData> = ({ items, title }) => {
  const classes = useSideCardStyles();

  return (
    <Box className={classes.sideBarBlock}>
      <h2 className={classes.title}>{title}</h2>
      {items.map((item) => {
        const { left, leftIcon, right } = item;
        return (
          <Grid key={left} container className={classes.item}>
            <Grid item xs={6} sm={4} md={5}>
              {left}
            </Grid>
            <Grid item xs={6} sm={8} md={7}>
              {Array.isArray(right) ? (
                right.map((r) => {
                  return (
                    <RightContainer
                      key={r.value}
                      display={r.display}
                      type={r.type}
                      value={r.value}
                      className={classes.adjacentLine}
                    />
                  );
                })
              ) : (
                <RightContainer key={right.value} display={right.display} type={right.type} value={right.value} />
              )}
            </Grid>
          </Grid>
        );
      })}
    </Box>
  );
};
