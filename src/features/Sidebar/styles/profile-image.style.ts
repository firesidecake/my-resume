import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    color: 'white',
    textAlign: 'center',
  },
  profileImage: {
    width: '18vw',
    minWidth: 202.9,
    height: '20vw',
    minHeight: 225,
    border: '0.25vw solid black',
    margin: 0,
  },
  name: {
    textTransform: 'uppercase',
    fontFamily: 'Raleway',
    fontSize: '200%',
    marginBottom: 0,
    paddingBottom: 0,
  },
  jobTitle: {
    fontFamily: 'Raleway',
    fontStyle: 'italic',
    fontSize: 16,
  },
}));
