import { makeStyles } from '@material-ui/core/styles';

import { navy } from '../../../styles/colors';

export const useStyles = makeStyles((theme) => ({
  sidebar: {
    order: 0,
    maxWidth: '33%',
    paddingTop: '3vw',
    paddingLeft: 'calc(2vw + 10px)',
    paddingRight: 'calc(2vw + 10px)',
    backgroundColor: navy[900],
    color: theme.palette.primary.contrastText,
    alignSelf: 'stretch',
    [theme.breakpoints.up('md')]: {
      overflow: 'auto', // TODO: Review this
    },
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      maxWidth: '100%',
    },
  },
}));
