import { makeStyles } from '@material-ui/core/styles';

export const useSideCardStyles = makeStyles({
  sideBarBlock: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    marginBottom: 25,
  },
  title: {
    textTransform: 'uppercase',
    fontFamily: 'Raleway',
    marginBottom: 10,
    marginTop: 0,
  },
  item: {
    '&:not(:last-child)': {
      marginBottom: 15,
    },
  },
  adjacentLine: {
    '&:not(:last-child)': {
      marginBottom: 5,
      display: 'block',
    },
  },
});

export const useSideInfoStyles = makeStyles({
  container: {},
});
