import React, { FC } from 'react';
import { Box, Typography } from '@material-ui/core';

import { SectionContainer } from './section-container.component';
import { useSharedStyles } from '../style/shared.style';
import { useSectionCardContainer } from '../style/section-card-container.style';

interface SectionCardContainer {
  mainTitle: string;
  description?: string;
}

export const SectionCardContainer: FC<SectionCardContainer> = ({ children, mainTitle, description }) => {
  const sharedClasses = useSharedStyles();
  const sectionCardClasses = useSectionCardContainer();

  return (
    <Box className={sectionCardClasses.sectionCardContainer}>
      <SectionContainer isPurple isLong>
        <Box>
          <Box>
            <Typography variant="h4" component="h2">
              {mainTitle}
            </Typography>
            <Typography variant="subtitle1">{description}</Typography>
          </Box>
        </Box>
      </SectionContainer>
      <SectionContainer className={sectionCardClasses.skillContainer}>
        <Box className={sharedClasses.container}>{children}</Box>
      </SectionContainer>
    </Box>
  );
};
