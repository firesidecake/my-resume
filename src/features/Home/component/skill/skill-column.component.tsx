import React, { FC, ReactElement, cloneElement } from 'react';
import { Grid, Box, Typography } from '@material-ui/core';
import clsx from 'clsx';

import { useSkillColumnStyles } from '../../style/skill-column.style';

interface SkillColumn {
  fullWidth?: boolean;
  hasBottomBorder?: boolean;
  icon: ReactElement;
  title: string;
}

export const SkillColumn: FC<SkillColumn> = ({ fullWidth, hasBottomBorder, icon, title, children }) => {
  const skillColumnClasses = useSkillColumnStyles();
  const sm = fullWidth ? 12 : 6;
  return (
    <Grid item xs={12} sm={sm} className={clsx(skillColumnClasses.container, hasBottomBorder && 'has-bottom')}>
      <Box className={skillColumnClasses.titleSection}>
        <Box className={skillColumnClasses.iconContainer}>
          {cloneElement(icon, { className: skillColumnClasses.icon })}
        </Box>
        <Typography className={skillColumnClasses.title}>{title}</Typography>
      </Box>
      {children}
    </Grid>
  );
};
