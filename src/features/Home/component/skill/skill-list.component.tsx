import React from 'react';
import { Card, Grid } from '@material-ui/core';

import { SectionCardContainer } from '../section-card-container.component';
import { OtherSkill, SkillCollaboration, SkillJs } from './skill-detail';
import { useSectionCardContainer } from '../../style/section-card-container.style';

const mainTitle = 'Skills & Proficiencies';
const description =
  '3 years as a computer-science student, 2.5 years since starting my first day as a professional developer plus multiple participation in programming competitions, I have been able to equip myself with:';

export const SkillList = () => {
  const sectionCardClasses = useSectionCardContainer();

  return (
    <SectionCardContainer mainTitle={mainTitle} description={description}>
      <Card className={sectionCardClasses.skillCardContainer}>
        <Grid container>
          <SkillJs />
          <SkillCollaboration />
        </Grid>
        <Grid container>
          <OtherSkill />
        </Grid>
      </Card>
    </SectionCardContainer>
  );
};
