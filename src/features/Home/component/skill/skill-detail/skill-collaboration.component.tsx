import React from 'react';
import { Link } from '@material-ui/core';
import { GitHub } from '@material-ui/icons';

import { SkillColumn } from '../skill-column.component';
import { SkillDetailWrapper } from './skill-detail-wrapper.component';

export const SkillCollaboration = () => {
  return (
    <SkillColumn hasBottomBorder title="Code Management" icon={<GitHub />}>
      <SkillDetailWrapper title="Code Management">
        <li>
          <b>Git</b>:
          <ul className="innerList">
            <li>
              Following{' '}
              <b>
                <Link target="b" href="https://www.atlassian.com/git/tutorials/comparing-workflows">
                  Git Workflow
                </Link>
              </b>{' '}
              recommended by Atlassian
            </li>
            <li>
              Task tracking with{' '}
              <b>
                <Link href="https://www.atlassian.com/software/jira" target="b">
                  Jira
                </Link>
              </b>{' '}
              and{' '}
              <b>
                <Link href="https://www.atlassian.com/software/confluence" target="b">
                  Confluence
                </Link>
              </b>
            </li>
          </ul>
        </li>
        <li>
          <b>Code practices</b>:
          <ul className="innerList">
            <li>
              Following efficient code practices with{' '}
              <b>
                <Link href="https://eslint.org/" target="b">
                  Eslint
                </Link>
              </b>
              /
              <b>
                <Link href="https://prettier.io/" target="b">
                  Prettier
                </Link>
              </b>
            </li>
            <li>Type checking with TypeScript</li>
          </ul>
        </li>
      </SkillDetailWrapper>
      <SkillDetailWrapper title="Remote Collaboration">
        <li>
          <b>Communication</b>: Via Slack, Zoom and Microsoft Team
        </li>
      </SkillDetailWrapper>
      <SkillDetailWrapper title="Dev tools">
        <li>
          <b>Editor/IDE:</b>{' '}
          <b>
            <Link href="https://www.jetbrains.com/idea/" target="b">
              IntelliJ
            </Link>
          </b>{' '}
          and{' '}
          <b>
            <Link href="https://code.visualstudio.com/" target="b">
              VSCode
            </Link>
          </b>
        </li>
        <li>
          <b>Git:</b>{' '}
          <b>
            <Link href="https://www.sourcetreeapp.com/" target="b">
              Sourcetree
            </Link>
          </b>
          ,{' '}
          <b>
            <Link href="https://www.jetbrains.com/idea/" target="b">
              IntelliJ
            </Link>
          </b>{' '}
          and Terminal
        </li>
      </SkillDetailWrapper>
    </SkillColumn>
  );
};
