import React, { FC } from 'react';
import { Typography } from '@material-ui/core';

import { useSkillColumnContentStyles } from '../../../style/skill-column.style';

interface SkillDetailWrapper {
  title: string;
}

export const SkillDetailWrapper: FC<SkillDetailWrapper> = ({ title, children }) => {
  const classes = useSkillColumnContentStyles();

  return (
    <section className={classes.subContainer}>
      <Typography className={classes.subTitle}>{title}</Typography>
      <ul className={classes.ulContainer}>{children}</ul>
    </section>
  );
};
