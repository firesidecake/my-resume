import React from 'react';
import { DeveloperMode } from '@material-ui/icons';
import { Link } from '@material-ui/core';

import { SkillColumn } from '../skill-column.component';
import { SkillDetailWrapper } from './skill-detail-wrapper.component';

export const SkillJs = () => {
  return (
    <SkillColumn hasBottomBorder title="JavaScript Ecosystem" icon={<DeveloperMode />}>
      <SkillDetailWrapper title="Frontend">
        <li>
          <b>React</b>:
          <ul className="innerList">
            <li>
              Server side rendering (SSR) with{' '}
              <b>
                <Link href="https://nextjs.org" target="b">
                  Next.js
                </Link>
              </b>
            </li>
            <li>
              State management with{' '}
              <b>
                <Link href="https://reactjs.org/docs/hooks-intro.html" target="b">
                  React Hooks
                </Link>
              </b>
              ,{' '}
              <b>
                <Link href="https://redux.js.org/" target="b">
                  Redux
                </Link>
              </b>{' '}
              and{' '}
              <b>
                <Link href="https://reactjs.org/docs/context.html" target="b">
                  React Context
                </Link>
              </b>
            </li>
            <li>
              Develop a cross-platform mobile app with{' '}
              <b>
                <Link href="https://reactnative.dev/" target="b">
                  React-Native
                </Link>
              </b>
            </li>
          </ul>
        </li>
        <li>
          <b>Data communication:</b>
          <ul className="innerList">
            <li>
              Data communication with REST API and GraphQL (using{' '}
              <b>
                <Link href="https://www.apollographql.com/" target="b">
                  Apollo Client
                </Link>
              </b>
              )
            </li>
            <li>
              Testing APIs with{' '}
              <b>
                <Link href="https://mochajs.org/" target="b">
                  Mocha.js
                </Link>
              </b>{' '}
              and API testing tool{' '}
              <b>
                <Link href="https://www.postman.com/" target="b">
                  Postman
                </Link>
              </b>
            </li>
          </ul>
        </li>
        <li>
          <b>UI, UX:</b>
          <ul className="innerList">
            <li>CSS, Sass</li>
            <li>
              Responsive design with native Flexbox and libraries such as{' '}
              <b>
                <Link href="https://material-ui.com/" target="b">
                  Material-UI
                </Link>
              </b>
              ,{' '}
              <b>
                <Link href="https://ant.design/" target="b">
                  Ant Design
                </Link>
              </b>{' '}
              and{' '}
              <b>
                <Link href="https://getbootstrap.com/" target="b">
                  Bootstrap
                </Link>
              </b>
            </li>
          </ul>
        </li>
        <li>
          <b>Angular, Electron</b>
        </li>
        <li>
          <b>Reactive Programming</b> with{' '}
          <b>
            <Link href="https://rxjs-dev.firebaseapp.com/" target="b">
              RxJS
            </Link>
          </b>
        </li>
      </SkillDetailWrapper>
      <SkillDetailWrapper title="Backend">
        <li>
          <b>Node.js</b>
          <ul className="innerList">
            <li>
              Building and refactoring APIs for the server written with{' '}
              <b>
                <Link href="https://expressjs.com/" target="b">
                  Express.js
                </Link>
              </b>
            </li>
            <li>
              Manage database with MySQL, PostgreSQL and{' '}
              <b>
                <Link href="https://sequelize.org/v5/" target="b">
                  Sequelize ORM
                </Link>
              </b>
            </li>
          </ul>
        </li>
      </SkillDetailWrapper>
      <SkillDetailWrapper title="TypeScript">
        <li>
          I write codes in{' '}
          <b>
            <Link href="https://www.typescriptlang.org/" target="b">
              TypeScript
            </Link>
          </b>{' '}
          due to its stability and reliability, along with its better support for IntelliSense
        </li>
      </SkillDetailWrapper>
    </SkillColumn>
  );
};
