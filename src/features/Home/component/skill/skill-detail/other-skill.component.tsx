import React from 'react';
import { Link } from '@material-ui/core';
import { Book } from '@material-ui/icons';

import { SkillColumn } from '../skill-column.component';
import { SkillDetailWrapper } from './skill-detail-wrapper.component';

export const OtherSkill = () => {
  return (
    <SkillColumn fullWidth title="Personal skills" icon={<Book />}>
      <SkillDetailWrapper title="Programming">
        <li>
          <b>Java</b>: I have three years of using Java throughout university and the workplace. I
          frequently participate in hackathons including the Kingdomhack Hackathon, and have received 10th place in
          Australia in theIEEEXtreme Programming Competition (using Java). More recently, I've used Java to implement
          cryptography & security algorithms for a project in my University major.
        </li>
      </SkillDetailWrapper>
      <SkillDetailWrapper title="Software">
        <li>
          <b>Code practices</b>:
          <ul className="innerList">
            <li>
              <b>Microsoft Office</b>, <b>Microsoft Visio</b> and <b>Microsoft Project:</b> Used to complete past
              university projects.
            </li>
            <li>
              Built website and mobile app interface prototypes with{' '}
              <Link href="https://www.axure.com/" target="b">
                <b>Axure</b>
              </Link>{' '}
              and{' '}
              <Link href="https://www.adobe.com/au/products/xd.html" target="b">
                <b>Adobe XD</b>
              </Link>
            </li>
          </ul>
        </li>
      </SkillDetailWrapper>
    </SkillColumn>
  );
};
