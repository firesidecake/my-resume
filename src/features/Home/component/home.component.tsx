import React from 'react';
import { Box } from '@material-ui/core';

import { useStyles } from '../style/content.style';
import { SectionHeading, SkillList } from '.';
import { JobList } from './job';
import { EductionList } from './education';

export const Home = () => {
  const classes = useStyles();

  return (
    <Box className={classes.container}>
      <SectionHeading />
      <SkillList />
      <JobList />
      <EductionList />
    </Box>
  );
};
