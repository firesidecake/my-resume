import React from 'react';
import { Card } from '@material-ui/core';

import { JobCard } from './job-card';
import { SectionCardContainer } from '../section-card-container.component';
import { useSectionCardContainer } from '../../style/section-card-container.style';
import { jobList } from '../../data/experience-list';

const mainTitle = 'Work & Activities';
const description = 'I have worked in multiple different development roles';

export const JobList = () => {
  const sectionCardClasses = useSectionCardContainer();

  return (
    <SectionCardContainer mainTitle={mainTitle} description={description}>
      {jobList.map((job, index) => {
        return (
          // eslint-disable-next-line react/no-array-index-key
          <Card key={index} className={sectionCardClasses.skillCardContainer}>
            <JobCard image={job.logo} description={job.description} date={job.date} title={job.title} />
          </Card>
        );
      })}
    </SectionCardContainer>
  );
};
