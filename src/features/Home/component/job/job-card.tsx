import React, { FC, ReactNode } from 'react';
import { Box, Typography } from '@material-ui/core';

import { useJobCardStyles } from '../../style/job-card.style';

interface JobCard {
  image: ReactNode;
  title: string;
  date?: string;
  description: ReactNode;
}

export const JobCard: FC<JobCard> = ({ image, title, description, date }) => {
  const jobCardClasses = useJobCardStyles();

  return (
    <Box className={jobCardClasses.container}>
      <Box className={jobCardClasses.imageContainer}>{image}</Box>
      <Box className={jobCardClasses.contentContainer}>
        <Box className={jobCardClasses.skillDesc}>
          <Box className={jobCardClasses.roleRow}>
            <Typography component="h3" className={jobCardClasses.roleName}>
              {title}
            </Typography>
            <Typography component="h3" className={jobCardClasses.roleDate}>
              {date}
            </Typography>
          </Box>
          <Box className={jobCardClasses.roleDescription}>{description}</Box>
        </Box>
      </Box>
    </Box>
  );
};
