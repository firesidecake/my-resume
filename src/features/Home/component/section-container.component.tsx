import React, { FC } from 'react';
import clsx from 'clsx';

import { useStyles } from '../style/section-container.style';
import { useSharedStyles } from '../style/shared.style';

interface SectionContainer {
  isPurple?: boolean;
  isLong?: boolean;
  className?: string;
}

export const SectionContainer: FC<SectionContainer> = ({ children, className, isPurple, isLong }) => {
  const classes = useStyles();
  const sharedClasses = useSharedStyles();

  return (
    <section
      className={clsx(
        classes.sectionContainer,
        isPurple && classes.isPurple,
        isLong && classes.isLong,
        sharedClasses.containerPadding,
        className
      )}
    >
      {children}
    </section>
  );
};
