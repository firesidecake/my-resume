import React from 'react';
import { Card } from '@material-ui/core';

import { JobCard } from '../job/job-card';
import { SectionCardContainer } from '../section-card-container.component';
import { useSectionCardContainer } from '../../style/section-card-container.style';
import { educationList } from '../../data/experience-list';

const mainTitle = 'Education & Academic achievements';
const description =
  'After I completed my high school in Vietnam in June 2016, I traveled abroad to start my study in Australia';

export const EductionList = () => {
  const sectionCardClasses = useSectionCardContainer();

  return (
    <SectionCardContainer mainTitle={mainTitle} description={description}>
      {educationList.map((job, index) => {
        return (
          // eslint-disable-next-line react/no-array-index-key
          <Card key={index} className={sectionCardClasses.skillCardContainer}>
            <JobCard image={job.logo} description={job.description} date={job.date} title={job.title} />
          </Card>
        );
      })}
    </SectionCardContainer>
  );
};
