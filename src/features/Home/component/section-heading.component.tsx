import React from 'react';
import clsx from 'clsx';
import { Box, Typography, Link } from '@material-ui/core';

import { useStyles } from '../style/section-heading.style';
import { useSharedStyles } from '../style/shared.style';

export const SectionHeading = () => {
  const classes = useStyles();
  const sharedClasses = useSharedStyles();

  return (
    <Box className={clsx(classes.sectionHeading, sharedClasses.containerPadding)}>
      <Typography variant="h4" component="h2" className={sharedClasses.mainHeader}>
        Overview
      </Typography>
      <hr />
      <div>
        I am a software engineer who has had an opportunity to work in multiple software development roles. Having
        completed a bachelor degree in Computer Science at the University of Wollongong (UOW), majoring in Digital
        System Security, I have a strong foundation in knowledge and experience when carrying out software development
        projects. Over the past few years, I have had the chance to work in a diverse range of organisations involving
        the following:
      </div>
      <ul>
        <li>Responsive HTML/CSS</li>
        <li>Frontend techs: React, React-native, Angular, Electron</li>
        <li>Data communication with REST and GraphQL</li>
        <li>Reactive Programming: RxJS</li>
        <li>Node.js</li>
        <li>Java</li>
        <li>Code management with Git</li>
      </ul>
      <br />
      <div>
        I am particularly enthusiatic about React, which I have been working with for the last 2.5 years. This portfolio
        was based on the{' '}
        <Link href="https://amakawa.info" target="b">
          single-HTML-page portfolio
        </Link>{' '}
        that I first wrote 2 years ago during my university time. [
        <Link href="https://gitlab.com/firesidecake/single-html-resume" target="b">
          source code
        </Link>
        ]
        <br />
        <br />
        To practice my React skills, I have rebuilt it into this current portfolio, a Next.js-based project with
        Material UI. [
        <Link href="https://gitlab.com/firesidecake/my-resume" target="b">
          source code
        </Link>
        ]
      </div>
    </Box>
  );
};
