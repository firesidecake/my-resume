import { makeStyles } from '@material-ui/core/styles';

export const useJobCardStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
  },
  imageContainer: {
    display: 'inline-flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '25%',
    '& img': {
      maxHeight: '7vw',
      objectFit: 'contain',
      alignSelf: 'center',
      width: '70%',
    },
    '& .logo': {
      alignItems: 'inherit',
      justifyContent: 'inherit',
      display: 'inherit',
    },
  },
  contentContainer: {
    marginLeft: '2vw',
    alignSelf: 'center',
    width: '100%',
  },
  skillDesc: {
    margin: 20,
    marginLeft: 0,
    marginRight: 40,
    '& .justify': {
      textAlign: 'justify',
    },
    '& .small-list-item': {
      display: 'list-item',
      listStyleType: 'circle',
      marginLeft: 15,
    },
  },
  roleRow: {
    color: theme.palette.secondary.light,
    marginBottom: '1.5rem',
    display: 'flex',
    alignItems: 'flex-end',
  },
  roleName: {
    fontWeight: 600,
    fontSize: '1.25rem',
  },
  roleDate: {
    marginLeft: 'auto',
    fontWeight: 500,
    whiteSpace: 'nowrap',
  },
  roleDescription: {},
}));
