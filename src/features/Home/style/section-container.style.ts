import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  sectionContainer: {
    paddingTop: '3rem',
    paddingBottom: '3rem',
  },
  isPurple: {
    backgroundColor: theme.palette.secondary.main,
    color: 'white',
  },
  isLong: {
    paddingBottom: '13rem',
  },
}));
