import { makeStyles } from '@material-ui/core/styles';

export const useSkillColumnStyles = makeStyles((theme) => ({
  container: {
    borderBottom: '0px solid #E6ECF8',
    borderRight: '1px solid #E6ECF8',
    padding: '45px 30px',
    '&.has-bottom': {
      borderBottom: '1px solid #E6ECF8',
    },
  },
  titleSection: {
    marginBottom: '1.5rem',
    textAlign: 'center',
  },
  iconContainer: {
    margin: '0 auto 1rem',
  },
  icon: {
    fontSize: 48,
    color: theme.palette.secondary.main,
  },
  title: {
    fontWeight: 800,
  },
}));

export const useSkillColumnContentStyles = makeStyles((theme) => ({
  subContainer: {
    '&:not(:last-child)': {
      marginBottom: '1rem',
    },
  },
  subTitle: {
    color: theme.palette.secondary.light,
    marginBottom: '0.5rem',
  },
  ulContainer: {
    listStylePosition: 'inside',
    paddingLeft: 0,
    '& li': {
      marginBottom: '0.25rem',
    },
    '& .innerList': {
      paddingLeft: 34,
      marginTop: '0.25rem',
      listStylePosition: 'outside',
    },
  },
}));
