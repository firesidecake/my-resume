import { makeStyles } from '@material-ui/core/styles';

export const useSharedStyles = makeStyles((theme) => ({
  containerPadding: {
    paddingLeft: 'calc(2vw + 10px)',
    paddingRight: 'calc(2vw + 10px)',
  },
  container: {
    margin: '0 auto',
    width: 'auto',
    position: 'relative',
    [theme.breakpoints.up('md')]: {
      marginLeft: -15,
      marginRight: -15,
    },
  },
  mainHeader: {
    color: theme.palette.secondary.main,
  },
}));
