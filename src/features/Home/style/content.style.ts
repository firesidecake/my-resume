import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  container: {
    order: 2,
    paddingTop: 'calc(3vw + 15px)',
    flex: 1,
  },
});
