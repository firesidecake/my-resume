import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  sectionHeading: {
    marginBottom: '2rem',
  },
});
