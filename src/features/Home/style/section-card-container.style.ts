import { makeStyles } from '@material-ui/core/styles';

export const useSectionCardContainer = makeStyles({
  sectionCardContainer: {
    '&:not(:last-child)': {
      marginBottom: '2rem',
    },
  },
  skillContainer: {
    marginTop: '-12rem',
    paddingBottom: '1rem',
  },
  skillCardContainer: {
    marginBottom: '2rem',
  },
});
