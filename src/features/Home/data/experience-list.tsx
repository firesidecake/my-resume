import React, { ReactNode } from 'react';
import { Link } from '@material-ui/core';

export type Job = {
  logo: ReactNode;
  title: string;
  date?: string;
  description: ReactNode;
};

export const jobList: Job[] = [
  {
    logo: (
      <Link className="logo" href="https://leapdev.io/" target="b">
        <img src="leapdev.png" alt="Leapdev logo" />
      </Link>
    ),
    date: 'July 2019 – April 2020',
    title: 'Junior Software Engineer',
    description: (
      <div className="justify">
        In July 2019, I joined the frontend development team of LEAP Dev. My role was to build the new version of the
        ByLaywers web application, which is one of the main website of LEAP. The website was implemented with
        server-side rendering with Next.js, a React-based framework. Data communication is with GraphQL (via Apollo
        Client) and REST API
      </div>
    ),
  },
  {
    logo: (
      <Link className="logo" href="http://www.itree.com.au/" target="b">
        <img src="itree-logo.png" alt="itree logo" />
      </Link>
    ),
    date: 'February 2019 - July 2019',
    title: 'Graduate Software Engineer',
    description: (
      <div className="justify">
        In the late of February 2019, I started a new role as a Graduate software engineer at{' '}
        <Link href="http://www.itree.com.au/" target="b">
          Itree
        </Link>
        . My rule was to set up configurations and rules for the internal platform in Itree
      </div>
    ),
  },
  {
    logo: (
      <Link className="logo" href="https://devika.com/" target="b">
        <img src="devika-logo.png" alt="devika logo" />
      </Link>
    ),
    date: 'November 2018 - Feb 2019',
    title: 'Frontend Web Developer',
    description: (
      <div className="justify">
        My main role is developing the new interface for{' '}
        <Link href="https://equalution.com" target="_blank">
          <b>Equalution.com</b>
        </Link>
        . The website is developed with ReactJS and a React-based template,{' '}
        <Link target="_blank" href="https://wrappixel.com/demos/admin-templates/material-pro/landingpage/index.html">
          MaterialPro
        </Link>
        . As a member in a team of 3 developers, I am the main frontend developer.
        <br />
        <br />
        In this job, I utilized Jira dashboard, Slack and Google Team Drive as the main means of communication within
        the team and with the clients.
      </div>
    ),
  },
  {
    logo: (
      <Link className="logo" href="https://www.linkedin.com/company/1scope/" target="b">
        <img src="1Scope-logo.png" alt="1Scope logo" />
      </Link>
    ),
    date: 'November 2017 - November 2018',
    title: 'Web Developer',
    description: (
      <div className="justify">
        1Scope is a single page application developed using{' '}
        <Link href="https://nodejs.org/en/" target="b">
          Node.js
        </Link>{' '}
        and{' '}
        <Link href="https://reactjs.org/" target="b">
          React.js
        </Link>{' '}
        technologies. It has server-side rendering for improved SEO. As a developer at 1Scope, I worked on both front
        and back-end of the system. My role ranged from building new APIs, writing test cases for them, building new
        components for the front-end of the system and bug fixing. I keep track of my work by managing active sprints
        with Jira, as well as keeping production codebase and mine updated via Bitbucket.
      </div>
    ),
  },
  {
    logo: (
      <Link
        className="logo"
        href="https://www.uow.edu.au/student/careers/work-integrated-learning/univative/"
        target="b"
      >
        <img src="UOWx-logo.jpg" alt="iUnivative - UOWx" />
      </Link>
    ),
    date: 'June - July 2017',
    title: 'iUnivative Contestant',
    description: (
      <div className="justify">
        Providing initiatives to increase the download counts for the mobile applications of{' '}
        <Link href="https://www.illawarrataxinetwork.com.au/" target="b">
          Illawarra Taxi Network (ITN)
        </Link>
        <br />
        <br />
        Developed simple prototypes for the application with{' '}
        <Link href="https://sketchapp.com/" target="b">
          Sketch
        </Link>
        .
        <br />
        <br />I was personally contacted by ITN to develop a new mobile app for the company afterwards. I decided to
        select developing a mobile app for ITN as the main topic of final final year university project
      </div>
    ),
  },
  {
    logo: <img src="UOW-logo.png" alt="UOW logo" />,
    date: 'Feb 2018 to April 2019',
    title: 'MakerSpace Mentor',
    description: (
      <div className="justify">
        As a UOW MakerSpace mentor, I was given a training how to learn basic usage of a 3D printer, VR devices (
        <Link href="https://www.oculus.com/" target="_b">
          Oculus Rift
        </Link>{' '}
        and{' '}
        <Link href="https://www.vive.com/au/" target="_b">
          HTC Vive
        </Link>
        ). My duty was to welcome students to the space, instruct them how to correctly interact with each device, to
        ensure students to follow the safety rules when coming to dangerous areas and to help students book the devices
        before coming in.
      </div>
    ),
  },
  {
    logo: <img src="UOW-wellbeing.jpg" alt="UOW Wellbeing logo" />,
    date: 'April 2017 to November 2017',
    title: 'UOW Wellbeing Ambassador',
    description: (
      <div className="justify">
        I became the ambassador of the Student Life & Wellbeing Hub at UOW last year. In this role, I was given the
        chance to experience a Mental health first aid training, to deliver a speech to a large audience in lectures to
        promote the activities of the hub, and to help the hub with the holding of many activities throughout the
        session.
      </div>
    ),
  },
  {
    logo: (
      <Link className="logo" href="https://ieeextreme.org/" target="b">
        <img src="IEEExtreme-logo.png" alt="IEEExtreme logo" />
      </Link>
    ),
    date: 'October 2017',
    title: 'IEEEXtreme Programming Competition 11.0',
    description: (
      <div className="justify">
        Joining in a team of 3 people, I experienced my second 24-hour programming competition with a lot of challenges
        and difficulties. The competition was held globally and each team had to strive to answer as many questions as
        possible. The main focus of the competition was providing most optimal answer to different algorithm problems.
        After 24 hours of relentless efforts, my team has achieved the 10th place in Australia.
      </div>
    ),
  },
  {
    logo: <img src="kingdomhack-logo.png" alt="Kingdomhack logo" />,
    date: 'November 2016',
    title: 'Kingdom Hack Wollongong',
    description: (
      <div className="justify">
        The first 24-hour Hackathon event that I attended, only after 4 months since the first day of my programming
        journey. In a team of 3 people where I was the only coder, I made a small Java application that was able to
        modify the host file to block particular pre-determined websites.
      </div>
    ),
  },
  {
    logo: <img src="other-activities-logo.png" alt="Other Activities" />,
    title: 'Other Activities',
    description: (
      <div className="justify">
        <span>I have also been engaged in many other activities:</span>
        <div className="small-list-item" style={{ marginLeft: 30, paddingTop: 10 }}>
          <b>UniCrew:</b> Participated in the UniCrew volunteering program
        </div>
        <div className="small-list-item" style={{ marginLeft: 30 }}>
          <b>Global Communication Program:</b> Enhancing my communication skill with foreign friends
        </div>
        <div className="small-list-item" style={{ marginLeft: 30 }}>
          <b>Community Volunteering Project 2017:</b> Working with other team members to raise fund for the Australian
          Indigenous Mentoring Experience (AIME).
        </div>
        <div className="small-list-item" style={{ marginLeft: 30 }}>
          <b>Welcome to Wollongong evening 2018 committee member:</b> Collaborating with other members to organize the
          traditional costume parade in the event.
        </div>
      </div>
    ),
  },
];

export const educationList: Job[] = [
  {
    logo: (
      <Link className="logo" href="https://www.uow.edu.au/" target="b">
        <img src="UOW-logo.png" alt="UOW logo" />
      </Link>
    ),
    date: 'July 2016 – July 2019',
    title: 'Bachelor of Computer Science',
    description: (
      <div className="justify">
        <div className="small-list">Achievements:</div>
        <div className="small-list-item" style={{ marginLeft: 30, paddingTop: 10 }}>
          <b>Graduated with Distinction,</b> with a major in Digital System Security
        </div>
        <div className="small-list-item" style={{ marginLeft: 30 }}>
          <b>Undergraduate Excellence Scholarship:</b> offered by University of Wollongong
        </div>
        <div className="small-list-item" style={{ marginLeft: 30 }}>
          <b>Faculty Merit Scholarship:</b> offered by faculty Engineering and Information Sciences, University of
          Wollongong
        </div>
        <div className="small-list-item" style={{ marginLeft: 30 }}>
          <b>Dean's merit Award 2016:</b> offered by the University of Wollongong for top 5% students with the higest
          marks within the faculty
        </div>
      </div>
    ),
  },
];
