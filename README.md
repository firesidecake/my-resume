# Quang Trung's Resume

My personal resume, built with Next.js.

## Getting Started

This is my personal resume.  

### Prerequisites

Node.js, Intellij which supports Eslint + Prettier (Needs to allow Intellij to use Eslint)

### Installing

To run the development build

```
yarn dev
```

Build prod

```
yarn start
```
